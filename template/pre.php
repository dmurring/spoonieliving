<!-- This file is part of the site's template; see /template/README.md for more information -->

</head>

<body>

<div class="container">

	<div class="row">

		<!-- Header -->
		<div id="header">
			<div class="col-xs-12 col-md-12">
				<a href="https://blog.spoonieliving.com">

					<!-- Mobile-responsive header image -->
					<picture>
						<source srcset="/img/header.png" media="(min-width: 800px)" alt="Image of a 50s housewife holding a spoon. Spoonie Living: 7 years of chronic illness and disability resources.">
							<source srcset="/img/headermobile.png">
								<img srcset="/img/headermobile.png" alt="Image of a 50s housewife holding a spoon. Spoonie Living: 7 years of chronic illness and disability resources.">
							</picture>

						</a>
					</div>
				</div>

				<!-- Navigation -->
				<div class="row">
					<div class="col-xs-12 col-md-12">
						<div id="navigation" class="navbar navbar-default" role="navigation">

							<!-- Mobile navigation with hamburger menu -->
							<div class="navbar-header">
								<button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target=".navbar-collapse" aria-label="Navigation">

									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>

								</button>

							</div>

							<!-- Main navigation -->
							<div class="navbar-collapse collapse">
								<ul class="nav navbar-nav">

									<!-- Navigation items -->
									<li class="nav-item"><a href="..">Welcome</a></li>
									<li class="nav-item"><a href="../about">About</a></li>

									<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Blog<b class="caret"></b></a>


										<ul class="dropdown-menu">
                                            <li><b><font color=#8C4646>&nbsp;&nbsp;Navigation</b></font></li>
											<li><a href="https://blog.spoonieliving.com/">Blog Home</a></li>
                                            <li><a href="/directory">Tag Directory</a></li>
                                            <li>&nbsp;</li>
                                            <li><b><font color=#8C4646>&nbsp;&nbsp;Recommended tags</b></font></li>
											<li><a href="https://blog.spoonieliving.com/tagged/original+content">Original Content</a></li>
											<li><a href="https://blog.spoonieliving.com/tagged/advice">Advice</a></li>
											<li><a href="https://blog.spoonieliving.com/tagged/review">Reviews</a></li>
											<li><a href="https://blog.spoonieliving.com/tagged/diy">DIY</a></li>
											<li><a href="https://blog.spoonieliving.com/tagged/self+care">Self Care</a></li>
											<li><a href="https://blog.spoonieliving.com/tagged/dealing+with+doctors">Dealing With Doctors</a></li>
										</ul>

									</li> <!-- End blog dropdown -->

									<li class="nav-item"><a href="https://dmurring.itch.io/chronicallybadass">Zine</a></li>

									<li class="nav-item"><a href="/featured">Featured Blogs</a></li>

								</ul>

								<!-- Search bar -->
								<form class="navbar-form navbar-right" role="search" onsubmit="location.href='https://blog.spoonieliving.com/search/' + document.getElementById('keywordInput').value; return false;">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Search"
										id="keywordInput">
									</div>
									<button type="submit" class="btn btn-default">&#128269;</button>
								</form>

							</div> <!-- End main navigation -->

						</div> <!-- End all navigation -->

					</div> <!-- End column -->

				</div> <!-- End header -->

			</div> <!-- End row -->


			<div class="row">

				<!-- Mobile ad banner 
				<div id="mobile-ad-banner-inline" class="hidden-lg hidden-md">
				  <center>
						<h5>(Just a quick advertisement before your resources!)</h5>

						<!-- Medivizor widget 
						<script type="text/javascript" id="medivizor_widget_2" class="dclg-impact-indicators-async-script-loader">
      			(function() {
		          function async_load(){
		              var s = document.createElement('script');
		              s.type = 'text/javascript';
		              s.async = true;
		              var theUrl = 'https://medivizor.com/partner/widget.js?id=eNuKDU78cC22TBaJLdo5';
		              s.src = theUrl + ( theUrl.indexOf("?") >= 0 ? "&" : "?") + 'ref=' + encodeURIComponent(window.location.href);
		              var embedder = document.getElementById('medivizor_widget_2');
		              embedder.parentNode.insertBefore(s, embedder);
						      }
						          if (window.attachEvent)
						              window.attachEvent('onload', async_load);
						          else
						              window.addEventListener('load', async_load, false);
							})();
  					</script>
  					<div id="medivizor_widget_eNuKDU78cC22TBaJLdo5" style="width:inherit"></div>

                -->

				</div>

				<!-- Content -->
				<div id="content">
					<div class="col-xs-12 col-md-12">
