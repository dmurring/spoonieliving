<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);

include "$root/template/head.php";
?>

<title>Spoonie Living</title>
<meta name="Description" content="Disability and Chronic Illness Lifestyle">
<meta name="Keywords" content="spoonie, chronic illness, disability, invisible illness, chronic pain, mental illness">

<?php
include "$root/template/pre.php";
?>

<div id="section">
	<h1>About</h1>
	<hr>
	<img class="textwrap-image-left" src="/img/baby.png" alt="Baby with spoon" width="200">
	<p>Spoonie Living is a lifestyle blog à la Martha Stewart Living for folks with chronic illness and/or disabilities who aren’t going to let their health problems keep them from living a fantastic life. Here you’ll find <a href="http://www.butyoudontlooksick.com/articles/written-by-christine/the-spoon-theory/">spoonie</a>-specific advice, products, reviews, as well as life hacks and awesome mods of all sorts of medical devices&mdash;so <a href="https://blog.spoonieliving.com">have a look</a> and start living the spoonie life with us!</p>

    <p><i>Friendly note: Spoonie Living has mostly stopped updating, starting in 2020. I'll be leaving it up as an archive, and may drop occasional posts, but don't intend to return to regular updates. As time goes on, links to outside sites may break; I recommend running those links through <a href="http://web.archive.org/">the Wayback Machine</a>, which is a great way to find web content that's no longer available. I will also be closing the contact email for the spoonieliving.com domain, but you can find me at <a href="http://www.dmurring.com">my professional site</a>!</i><br><br></p>
	</div>
</div>

<div id="section">
	<h3>The Editor</h3>
	<img class="textwrap-image-right" src="/img/headshot.png" alt="Diane Murray" width="150">
	<p>Spoonie Living is curated by <a href="http://www.dmurring.com">Diane Murray</a>, a spoonie from Portland, OR with a passion for creating resources on topics of all kinds.</p>
	<p>Diane has a <a href="https://en.wikipedia.org/wiki/Connective_tissue_disease">connective tissue disorder</a>, <a href="https://en.wikipedia.org/wiki/Postural_orthostatic_tachycardia_syndrome">Postural Orthostatic Tachycardia Syndrome</a>, <a href="https://en.wikipedia.org/wiki/Chronic_fatigue_syndrome">Systemic Exertion Intolerance Disease/Chronic Fatigue Syndrome</a>, <a href="https://en.wikipedia.org/wiki/Small_intestinal_bacterial_overgrowth">Small Intestine Bacterial Overgrowth</a>, probable <a href="https://en.wikipedia.org/wiki/Mast_cell_activation_syndrome">Mast Cell Activation Syndrome</a>, and <a href="https://en.wikipedia.org/wiki/Cyclothymia">Cyclothymia</a>, and expects their collection to only grow as time goes on.</p>

</div>

<div id="section">
	<h3>Inclusion &amp; Social Justice</h3>
	<p>We know what oppression feels like firsthand, and make it our goal to keep this blog a safe space for all marginalized groups&mdash;chronically ill and otherwise. Spoonie Living practices compassionate intersectional feminism by avoiding racist, appropriative, cissexist, and other -ist language in our original content, and tagging for the same in the content of others. And in case it doesn't go without saying, TERF ideology is not welcome.</p>
</div>

<div id="section">
	<h3>FAQ</h3>

	<h4><font color="black">💊</font> What’s all this with the spoons?</h4>
	<p>The term “spoonie” refers to a person with a chronic illness of some type (mental or physical) that has a serious impact on their daily life. It comes from <a href="http://	www.butyoudontlooksick.com/articles/written-by-christine/the-spoon-theory/">Christine Miserando’s Spoon Theory story</a>, and it resonated with so many people that it spawned its own term and is used by folks all over the world!</p>
	<br>

	<h4><font color="black">💊</font> Do I count as a spoonie?</h4>
	<p>We get this question all the time, and the answer is: if you’re asking, you probably do! If you spend a significant time during your average day caring for yourself or measuring out your emotional and/or physical resources to decide what you can and can’t afford to do&mdash;well, welcome to the world of spoonies.</p>

	<p><a href="https://blog.spoonieliving.com/post/158859942465/am-i-a-spoonie-an-interactive-guide">If you’re not convinced you belong in the spoonie community, here is a silly interactive guide!</a></p>
	<br>
</div>

<div id="section">
<h3>Disclosure</h3>

<p>You should be aware that this blog accepts forms of cash advertising, sponsorship, paid insertions or other forms of compensation. We are sometimes compensated to provide opinion on products, services, websites, and various other topics.</p>

	<p>The compensation received will never influence the content, topics or posts made in this blog. Any advertisements will be clearly identified as such, as will sponsored posts and reviews. We use the tags #sponsored and #advertisement so that readers can easily filter to opt out of seeing these posts.</p>

	<p>The views and opinions expressed on this blog are purely our own. Any product claim, statistic, quote or other representation about a product or service should be verified with the manufacturer, provider, or party in question. This blog does not contain any content which might present a conflict of interest.</p>

	<p><b>We choose to promote only products that are in line with our blog's values and we feel will truly benefit our readers. In our reviews, we can assure you that we’ll always give our honest opinions, findings, beliefs, or experiences on those topics or products.</b></p>

	<p>Please feel free to <a href="mailto:dmurring@gmail.com">contact the editor</a> with any additional questions or concerns about this blog!
	</p>

</div>

<?php
include "$root/template/post.php";
?>
