<!-- Template: head.php -->
<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include "$root/template/head.php";
?>

<!-- Metadata -->
<title>Tag Directory</title>
<meta name="Description" content="Tag directory for Spoonie Living">
<meta name="Keywords" content="spoonie, chronic illness, disability, invisible illness, chronic pain, mental illness">

<!-- Template: pre.php -->
<?php
include "$root/template/pre.php";
?>

<!-- Content -->

<div>

	<!-- Content header -->
	<h1>Tag Directory</h1>
	<hr>
	<br>
    <p>Click any header to see the relevant tag list. These are the ones that were commonly used during the life of the blog, but feel free to use the search in the navigation bar for keyword searches.</p>

<p>You can also try the below tag search, since Tumblr's keyword search can be dodgy at times.</p>

<!-- Search bar -->
<form class="navbar-form" role="search" onsubmit="location.href='https://blog.spoonieliving.com/tagged/' + document.getElementById('myInput2').value; return false;">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Tag search"
										id="myInput2">
									</div>
									<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
								</form>

<br>

	<!-- Main content -->
    <button type="button" class="collapsible">+ Types of Posts</button>
    <div class="content">
       <br>
    <div class="columns">
        <p><a href="https://blog.spoonieliving.com/tagged/original%20content">Original content</a></p>
        <p><a href="https://blog.spoonieliving.com/tagged/psa">PSA</a></p>
        <p><a href="https://blog.spoonieliving.com/tagged/how%20to">How
        To</a></p>
        <p><a href="https://blog.spoonieliving.com/tagged/what%20to%20expect">What
        to Expect</a></p>
        <p><a href="https://blog.spoonieliving.com/tagged/blog">Blogs</a></p>
        <p><a href="https://blog.spoonieliving.com/tagged/interview">Interview</a></p>
        <p><a href="https://blog.spoonieliving.com/tagged/research">Research</a>
        (<a href="https://blog.spoonieliving.com/tagged/study">study</a>,
        <a href="https://blog.spoonieliving.com/tagged/lit%20review">lit
        review</a>)</p>
        <p><a href="https://blog.spoonieliving.com/tagged/product">Product</a></p>
        <p><a href="https://blog.spoonieliving.com/tagged/service">Service</a></p>
        <p><a href="https://blog.spoonieliving.com/tagged/review">Review</a></p>
        <p><a href="https://blog.spoonieliving.com/tagged/recommendation">Recommendation</a></p>
        <p><a href="https://blog.spoonieliving.com/tagged/app">App</a>
        (try also <a href="https://blog.spoonieliving.com/tagged/apps">apps</a>)
        (<a href="https://blog.spoonieliving.com/tagged/ios">iOS</a>,
        <a href="https://blog.spoonieliving.com/tagged/android">Android</a>)</p>
        <p><a href="https://blog.spoonieliving.com/tagged/masterpost">Masterpost</a>
        (<a href="https://blog.spoonieliving.com/tagged/linkup">linkup</a>)</p>
        <p><a href="https://blog.spoonieliving.com/tagged/ask">Ask</a></p>
        <p><a href="https://blog.spoonieliving.com/tagged/advice">Advice</a></p>
        <p><a href="https://blog.spoonieliving.com/tagged/audience%20participation">Audience
        participation </a>(crowdsourcing posts)</p>
        <p><a href="https://blog.spoonieliving.com/tagged/insight">Insight</a></p>
        <p><a href="https://blog.spoonieliving.com/tagged/validation">Validation</a></p>
        <p><a href="https://blog.spoonieliving.com/tagged/discourse">Discourse</a>
        (early posts used <a href="https://blog.spoonieliving.com/tagged/the%20discourse">the
        discourse</a>)</p>
        <p><a href="https://blog.spoonieliving.com/tagged/framework">Framework</a>
        (ways to make sense of things)</p>
        <p><a href="https://blog.spoonieliving.com/tagged/USA">USA</a></p>
        <p><a href="https://blog.spoonieliving.com/tagged/UK">UK</a></p>
        <br>
    </div>
			<br>
    </div>

    <button type="button" class="collapsible">+ Medical</button>
    <div class="content">
				<br>
        <div class="columns">

        <p class="tag-header">Physical conditions</p>
            <p><a href="https://blog.spoonieliving.com/tagged/chronic+pain">Chronic pain</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/chronic+fatigue">Chronic
            fatigue (general)</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/dysautonomia">Dysautonomia</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/POTS">POTS</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/cfs">ME/CFS</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/eds">EDS</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/mcas">MCAS</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/autoimmune">Autoimmune
            disease</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/lupus">Lupus</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/psoriasis">Psoriasis</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/ms">MS</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/sibo">SIBO</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/anaphylaxis">Anaphylaxis</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/disordered%20eating">Disordered
            eating</a> (<a href="https://blog.spoonieliving.com/tagged/eating%20disorder">eating
            disorder</a>)</p>
            <p><a href="https://blog.spoonieliving.com/tagged/vision">Vision</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/vision%20impairment">Vision impairment</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/incontinence">Incontinence</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/tremors">Tremors</a>
            (<a href="https://blog.spoonieliving.com/tagged/fine%20motor%20skills">fine motor skills)</a></p>
        </div>
        <br>

<div>

        <p class="tag-header">Mental/developmental/intellectual conditions</p>
            <p><a href="https://blog.spoonieliving.com/tagged/neurodivergent">Neurodivergent</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/autism">Autism</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/adhd">ADHD</a>
            (no ADD specific tag, I was young and foolish!)</p>
            <p><a href="https://blog.spoonieliving.com/tagged/sensory+processing+disorder">Sensory
            processing disorder</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/stimming">Stimming</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/mental%20health">Mental
            health</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/trauma">Trauma</a> (<a href="https://blog.spoonieliving.com/tagged/PTSD">PTSD</a>)</p>
            <p><a href="https://blog.spoonieliving.com/tagged/abuse">Abuse</a><br></p>
<br> <br>
</div>

			<div>
        <p class="tag-header">Medical system</p>
            <p><a href="https://blog.spoonieliving.com/tagged/hospital">Hospital</a>(<a href="https://blog.spoonieliving.com/tagged/hospitalization">hospitalization</a>)</p>
            <p><a href="https://blog.spoonieliving.com/tagged/er">ER</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/dealing%20with%20doctors">Dealing
            with doctors</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/pain%20scale">Pain
            scale</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/gatekeeping">Gatekeeping</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/fatphobia">Fatphobia</a></p>
        </div>
            <br>
							<br>
        <div>
        <p class="tag-header">Treatments</p>
            <p><a href="https://blog.spoonieliving.com/tagged/treatment">Treatment</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/medication">Medication</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/pharmaceutical">Pharmaceutical</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/side%20effects">Side
            effects</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/therapy">Therapy</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/marijuana">Marijuana</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/tubie">CBD</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/tubie">TPN
            &amp; HPN (Tubie)</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/cold%20therapy">Cold
            therapy</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/heating-pad">Heating
            pad</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/epinephrine">Epinephrine/epipen</a>
            (<a href="https://blog.spoonieliving.com/post/170809420397/epipen-affordability-resources-hey-friends-a">affordability
            masterpost</a>)</p>
        <br>

        </div>

    </div>

    </div>

    <button type="button" class="collapsible">+ Life and Lifestyle</button>
    <div class="content">
        <br>
    <div class="columns">

            <p><a href="https://blog.spoonieliving.com/tagged/friends%20and%20family">Friends
        and family</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/dating">Dating</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/self%20care">Self
            care</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/communication">Communication</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/tracking">Tracking</a>
            (<a href="https://blog.spoonieliving.com/tagged/health+tracking">he</a><a href="https://blog.spoonieliving.com/tagged/health+tracking">alth</a>,
            <a href="https://blog.spoonieliving.com/tagged/symptom+tracking">s</a><a href="https://blog.spoonieliving.com/tagged/symptom+tracking">ymptom</a>,
            <a href="https://blog.spoonieliving.com/tagged/medication+tracking">m</a><a href="https://blog.spoonieliving.com/tagged/medication+tracking">edication</a>)</p>
            <p><a href="https://blog.spoonieliving.com/tagged/planning">Planning</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/shopping">Shopping</a>
            (<a href="https://blog.spoonieliving.com/tagged/online%20shopping">online
            shopping</a>)</p>
            <p align="left"><a href="https://blog.spoonieliving.com/tagged/fashion">Fashion</a>
            (<a href="https://blog.spoonieliving.com/tagged/adaptive%20clothing">adaptive
            clothing</a>)</p>
            <p><a href="https://blog.spoonieliving.com/tagged/exercise">Exercise</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/mobility">Mobility</a>
            (<a href="https://blog.spoonieliving.com/tagged/wheelchair">wheelchair</a>,
            <a href="http://cane/">cane</a>, <a href="http://rollator/">rollator</a>)</p>
            <p><a href="https://blog.spoonieliving.com/tagged/home">Home</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/warming">Warming
            </a> (<a href="https://blog.spoonieliving.com/tagged/cold+weather">cold</a>)
            *<a href="https://blog.spoonieliving.com/post/167745706463/image-photograph-of-two-kitschy-plaster">cold
            weather masterpost</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/cooling">Cooling</a>
            (<a href="https://blog.spoonieliving.com/tagged/hot+weather">heat</a>)
            *<a href="https://blog.spoonieliving.com/post/174764443777/image-posed-kermit-and-pink-panther">hot
            weather masterpost</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/financial">Financial</a>
            (<a href="https://blog.spoonieliving.com/tagged/money">money</a>,
            <a href="https://blog.spoonieliving.com/tagged/medical%20bills">medical
            bills</a>, <a href="https://blog.spoonieliving.com/tagged/free">free</a>)</p>
            <p><a href="https://blog.spoonieliving.com/tagged/work">Work</a>
            (<a href="https://blog.spoonieliving.com/tagged/work%20from%20home">work
            from home</a>, <a href="https://blog.spoonieliving.com/tagged/employment">employment</a>,
            <a href="https://blog.spoonieliving.com/tagged/career">career</a>)</p>
            <p><a href="https://blog.spoonieliving.com/tagged/travel">Travel</a>
            (<a href="https://blog.spoonieliving.com/tagged/flying">flying</a>)</p>
            <p><a href="https://blog.spoonieliving.com/tagged/accessibility">Accessibility</a>
            (<a href="https://blog.spoonieliving.com/tagged/accessibility+guide">accessibility
            guides</a>)</p>
            <p><a href="https://blog.spoonieliving.com/tagged/legal">Legal</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/queer">Queer</a>
            (<a href="https://blog.spoonieliving.com/tagged/trans">trans</a>,
            <a href="https://blog.spoonieliving.com/tagged/nonbinary">nonbinary</a>)</p>
            <p><a href="https://blog.spoonieliving.com/tagged/activism">Activism</a></p>
            <p><a href="https://blog.spoonieliving.com/tagged/social%20justice">Social
            justice</a></p>

    </div>
    <br>
    </div>

    <button type="button" class="collapsible">+ Content Warning Tags</button>
    <div class="content">
    <br>
    <i>Note: this list is meant for Tumblr users who wish to add tags to
their block lists before browsing. These are things that have been
tagged for in the past, but they aren’t a guarantee and older posts
especially may not be tagged properly. You may want to consider
adding “CN [tag]” and “TW [tag]” to your block lists as well.</i></p>
<p><br/>

</p>
<div>
<p class="tag-header">Common triggers</p>
<div class="columns">
<p>Money</p>
<p>Self image</p>
<p>Food</p>
<p>Food image</p>
<p>Disordered eating</p>
<p>Fatphobia</p>
<p>Purging</p>
<p>Abuse</p>
<p>Incest</p>
<p>Death</p>
<p>Suicide/suicide
mention</p>
<p>Animal death</p>
<p>Meat image</p>
<p>Anatomy</p>
<p>Musculature</p>
<p>Bones</p>
<p>Viscera</p>
<p>Organs</p>
</div>
<br/>
</div>



<div class="row">

<div class="col-sm-12 col-md-6">
<p class="tag-header">Phobias</p>
<p>Trypophobia/trypophobia
warning</p>
<p>Emetophobia/emetophobia
warning</p>
<p>Scoptophobia/scopophobia
(inconsistent)</p>
<p>Glitter</p>
<p>Needles</p>
<br/>
</div>

<div class="col-sm-12 col-md-6">
<p class="tag-header">Marginalized
communities</p>
<p>Cissexism</p>
<p>Heterosexism</p>
<p>Ableism</p>
<p>Slur/reclaimed slur</p>
<p>Abelist
slur/reclaimed ableist slur</p>
<p>Misogynist
slur/reclaimed misogynist slur</p>
<p><br/>
</div>

</div>


</div>

<script>
var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}
</script>
<!-- End content -->



<!-- Template: post.php -->
<?php
include "$root/template/post.php";
?>
